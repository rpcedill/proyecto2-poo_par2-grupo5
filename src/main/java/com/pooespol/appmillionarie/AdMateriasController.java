/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie;

import com.pooespol.appmillionarie.modelo.Materia;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author raini
 */
public class AdMateriasController {         //ATRIBUTOS DE LA CLASE Y LA VENTANA
    private ArrayList<Materia> materias;
    @FXML
    private Button adPreguntas;
    @FXML
    private Button salir;
    @FXML
    private Button adParalelos;
    @FXML
    private TextField textoNombreMat;
    @FXML
    private TextField textoCodigoMat;
    @FXML
    private TextField textoNiveles;
    @FXML
    private Button agregarMateria;
    @FXML
    private TableColumn<Materia, String> colcodigo;
    @FXML
    private TableColumn<Materia, String> colnombre;
    @FXML
    private TableColumn<Materia, String> colniveles;
    @FXML
    private Button adTerminos;
    @FXML
    private TableView lasmaterias;
    @FXML
    private TableColumn<Materia, String> colestado;

    @FXML
    private void switchToAdPreguntas() throws IOException { //CAMBIAR A PREGUNTAS
        App.setRoot("adPreguntas");
    }
    
    @FXML
    private void switchToAdTerminos() throws IOException {              // CAMBIAR A TERMINOS
        App.setRoot("secondary");
    }
    
    @FXML
    private void switchToMenu() throws IOException {             // CAMBIAR A MENU
        App.setRoot("primary");
    }

    @FXML
    private void switchToAdParalelos() throws IOException {             // CAMBIAR A PARALELOS
        App.setRoot("adParalelos");
    }

    @FXML
    private void agregarMateria() {             // FUNCION AGREGAR MATERIA
        System.out.println("Guardando Materia");             // OBTENCION DE LO INGRESADO
        String nombre= textoNombreMat.getText();
        String codigo= textoCodigoMat.getText();
        String niveles = textoNiveles.getText();
        
        if(nombre.isEmpty() || codigo.isEmpty() || niveles.isEmpty()){             // VALIDACIONES DE VACIO O EXISTENTE
            Alert vacio= new Alert(Alert.AlertType.INFORMATION);
            vacio.setTitle("Informacion de campo no ingresado");             // ALERTA ERROR
            vacio.setHeaderText("Campo sin llenar");
            vacio.setContentText("Llene todos los campos de registro");
            vacio.showAndWait();
        }
        else if(materiaExiste(nombre, codigo)){             // COMPRUEBA SI LA MATERIA EXISTE
            System.out.println("La materia ingresada ya existe");             // ALERTA ERROR
            Alert alertIngreso = new Alert(Alert.AlertType.INFORMATION);
            alertIngreso.setTitle("Informacion de ingreso de materia");
            alertIngreso.setHeaderText("Ingresó informacion ya existente");
            alertIngreso.setContentText("Materia ingresada ya existe\nINGRESE UNO NUEVO");
            alertIngreso.showAndWait();          
            
        }else{             // CREACION DE MATERIA
            Materia mat= new Materia(nombre,codigo,true, Integer.valueOf (niveles));
            materias.add(mat);

            FileOutputStream listaout= null;             // INICIO DE SERIALIZACION MATERIA
            try{
            listaout= new FileOutputStream(App.pathMaterias);  //en el archivo que indica la ruta voy a guardar el archivo serializado
            ObjectOutputStream objlista = new ObjectOutputStream(listaout); //serializo la lista
            objlista.writeObject(materias); //escribo la lista en el archivo
            objlista.flush();             // FIN DE SERIALIZACION MATERIA

            Alert alert = new Alert(Alert.AlertType.INFORMATION);             // AVISO DE EXITO
                alert.setTitle("INFORMACION");
                alert.setHeaderText("Resultado de la operacion");
                alert.setContentText("Nueva materia agregado exitosamente");

                alert.showAndWait();             // REFRESCAR VENTANA
                App.setRoot("adMaterias");
            }catch(IOException ex){
                ex.printStackTrace();
                System.out.println("IOException:" + ex.getMessage());
            }finally{
                try{
                    listaout.close();
                }catch(IOException ex){
                    ex.printStackTrace();
                }
            }
        }
    }
    
    public boolean materiaExiste(String a, String b){             // COMPRUEBA MATERIAS EXISTENTES
        for (Materia m: materias){
            if (m.getNombre().equals(a) || m.getCodigo().equals(b)){
                return true;
            }
        }
        return false;
     }
        
    
    @FXML
    public void agregarImagenbtn(){             // AGREGAR IMAGENES DE LA VENTANA
        FileInputStream imageadd = null;
        try{
            imageadd = new FileInputStream(App.pathimagen+"add_cosa.png");
            ImageView imv = new ImageView(new Image(imageadd,50,50,false,false));
            agregarMateria.setGraphic(imv);
        
        }catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
    }

    public AdMateriasController() {             // CONTROLADOR, CARGAR DATOS DE MATERIAS
        Datos d1 = new Datos();
        materias = d1.cargarMaterias();
        
    }
    @FXML
    private void initialize() {             // ASIGNA Y LLENA EL TABLE VIEW Y AGREGA IMAGEN
        agregarImagenbtn();
        colnombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colcodigo.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        colestado.setCellValueFactory(new PropertyValueFactory<>("activa"));
        colniveles.setCellValueFactory(new PropertyValueFactory<>("cantNiveles"));

        lasmaterias.getItems().setAll(materias);
        
        
    }
    
}
