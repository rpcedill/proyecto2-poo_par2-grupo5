package com.pooespol.appmillionarie;
//autor gustav
import com.pooespol.appmillionarie.modelo.TerminoAcademico;
//import com.pooespol.appmillionarie.eventos.ManejaTerminosConf;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

public class SecondaryController {             // ATRIBUTOS DE LA CLASE Y VENTANA
    private ArrayList<TerminoAcademico> terminosAc;
    //ArrayList<TerminoAcademico> terserializado;
    private TerminoAcademico teraca;
    @FXML
    private TableView losterminos;
    @FXML
    private Label terminoJuego;
    @FXML
    private TableColumn<TerminoAcademico, String> colaño;
    @FXML
    private TableColumn<TerminoAcademico, String> colnumero;
    @FXML
    private Label labelterminos;
    private String[] numerTermino= {"1","2","3"};
    @FXML
    private ComboBox cmbNumerosTerm;
    @FXML
    private TextField textoaño;
    @FXML
    private Button btningresartermino;
    @FXML
    private FlowPane myflowpane;
    @FXML
    private Button admterminos;
    @FXML
    private Button adMatPar;
    @FXML
    private Button adPreguntas;
    @FXML
    private VBox myvbox;
    @FXML
    private Button btnconfigurarjuego;
    /*
    @FXML
    FlowPane cambiaflow;
    @FXML
    VBox estableceVbox;
    */
    @FXML
    private void switchToPrimary() throws IOException {             // CAMBIA AL MENU
        App.setRoot("primary");
    }
    @FXML
    private void switchToAdMaterias() throws IOException {             // CAMBIA A MATERIAS
        App.setRoot("adMaterias");
    }
    @FXML
    private void switchToAdPreguntas() throws IOException {             // CAMBIA A PREGUNTAS
        App.setRoot("adPreguntas");
    }
    public SecondaryController(){             // CONSTRUCTOR Y CARGA DE TERMINOS Y TERMINO CONFIGURADO
        Datos d1 = new Datos();
        terminosAc = d1.cargarTerminos();
        teraca = d1.cargarTerminoJuego();
    }
    
  

    @FXML
    private void initialize() {             // LLENA EL TABLE VIEW Y LOS COMBOBOX Y EL LABEL DEL TERMINO CONFIGURADO
        //cmbEstado.getItems().setAll(EstadoCivil.values());

        colaño.setCellValueFactory(new PropertyValueFactory<>("year"));
        colnumero.setCellValueFactory(new PropertyValueFactory<>("numeroTermino"));
        losterminos.getItems().setAll(terminosAc);
        cmbNumerosTerm.getItems().setAll(numerTermino);
        terminoJuego.setText(teraca.toSting());
        agregarImagenbtn();
        
    }
    
    
    @FXML
    public void ingresarTermino(){             // FUNCION INGRESAR TERMINO
        System.out.println("Guardando Termino Academico");             // OBTENCION DE LO INGRESADO
        String añotermino= textoaño.getText();
        String numbtermino= (String) cmbNumerosTerm.getValue();
        if(añotermino.isEmpty() || numbtermino==null){             // VALIDACION DE NULO O EXISTENTE
            Alert vacio= new Alert(Alert.AlertType.INFORMATION);             // ALERTA ERROR
            vacio.setTitle("Informacion de campo no ingresado");
            vacio.setHeaderText("Campo sin llenar");
            vacio.setContentText("Llene todos los campos de registro");
            vacio.showAndWait();
        }
        else if(terminoAcademicoExiste(añotermino, Integer.valueOf (numbtermino))){             // COMPRUEBA SI YA EXISTE EL TERMINO
            System.out.println("El termino ingresado ya existe");             // ALERTA ERROR
            Alert alertIngreso = new Alert(Alert.AlertType.INFORMATION);
            alertIngreso.setTitle("Informacion de ingreso de termino");
            alertIngreso.setHeaderText("Ingresó informacion ya existente");
            alertIngreso.setContentText("Termino ingresado ya existe\nINGRESE UNO NUEVO");
            alertIngreso.showAndWait();          
            
        }else{             // CREA EL TERMINO ACADEMICO
        TerminoAcademico ta= new TerminoAcademico(añotermino, Integer.valueOf (numbtermino));
        terminosAc.add(ta);
        //System.out.println("Nuevo Termino: "+ta); //me agrega a la lista pero no al archivo 
     
        FileOutputStream listaout= null;             // INICIO SERIALIZACION DE TERMINOS
        try{
        listaout= new FileOutputStream(App.pathTerminosAc);  //en el archivo que indica la ruta voy a guardar el archivo serializado
        ObjectOutputStream objlista = new ObjectOutputStream(listaout); //serializo la lista
        objlista.writeObject(terminosAc); //escribo la lista en el archivo
        objlista.flush();             // FIN DE SERIALIZACION DE TERMINOS
        
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("INFORMACION");             // ALERTA EXITO
            alert.setHeaderText("Resultado de la operacion");
            alert.setContentText("Nuevo termino agregado exitosamente");

            alert.showAndWait();
            App.setRoot("secondary");             // REFRESCA LA VENTANA
        }catch(IOException ex){
            ex.printStackTrace();
            System.out.println("IOException:" + ex.getMessage());
        }finally{
            try{
                listaout.close();
            }catch(IOException ex){
                ex.printStackTrace();
            }
        }
    }
        
    }
    public boolean terminoAcademicoExiste(String yy, int number){             // COMPRUEBA SI EXISTE EL TERMINO
        TerminoAcademico term = new TerminoAcademico(yy,number);
        return terminosAc.contains(term);
    }
    @FXML
    public void agregarImagenbtn(){             // AGREGA IMAGENES A LE VENTANA
        FileInputStream imageadd = null;
        FileInputStream imageconf=null;
        try{
            imageadd = new FileInputStream(App.pathimagen+"agregararchivo2.png");
            imageconf= new FileInputStream(App.pathimagen+"configurar4.png");
            Image imagenagregar = new Image(imageadd,42,42,false,false );
            ImageView imv = new ImageView(imagenagregar);
            btningresartermino.setGraphic(imv);
            btnconfigurarjuego.setGraphic(new ImageView(new Image(imageconf,45,47,false,false)));
        
        }catch(FileNotFoundException ex){
            ex.printStackTrace();
        }finally{
            try{
            imageadd.close();
            imageconf.close();
            }catch(IOException ex){
                ex.printStackTrace();
            }
        
        }
    }
    @FXML
    public void configTerminoJuego(){             // FUNCION CONFIGURAR TERMINO PARA JUEGO
        int filatermino = losterminos.getSelectionModel().getSelectedIndex();
        
        if(filatermino==-1){
            System.out.print("termino no seleccionado");             // NO SE SELECCIONO EN EL TABLE VIEW
            Alert alerta= new Alert(Alert.AlertType.INFORMATION);
            alerta.setTitle("informacion de seleccion de termino");
            alerta.setHeaderText("No se ha seleccionado termino");
            alerta.setContentText("Seleccione una fila de la table");
            alerta.showAndWait();
        
        }else{             // SER ASIGNO EL TERMINO SELECCIONADO COMO EL TERMINO PARA EL JUEGO
            TerminoAcademico terminoacad = (TerminoAcademico) losterminos.getSelectionModel().getSelectedItem();
            if(terminosAc.contains(terminoacad)){
            int indice = terminosAc.indexOf(terminoacad);
            teraca= terminosAc.get(indice);
            System.out.println(teraca);
            losterminos.setOnMouseClicked( new ManejaTerminoJuego(teraca));
            System.out.println("Se ha seleccionado termino para juego");
            terminoJuego.setText(teraca.toSting());
            Alert alertacorrecta= new Alert(Alert.AlertType.INFORMATION);
            alertacorrecta.setTitle("informacion de configuracion de termino academico");
            alertacorrecta.setHeaderText("usuario ha seleccionado un termino");
            alertacorrecta.setContentText("Se Ha configurado el termino academico para el juego\n"+teraca);
            alertacorrecta.showAndWait();
            ArrayList<TerminoAcademico> termi = new ArrayList<>();
            termi.add(teraca);
            FileOutputStream listaout= null;             // INICIO DE SERIALIZACION TERMINO JUEGO CONFIGURADO
                try{ 
                    listaout = new FileOutputStream(App.pathTerminoJuego); 
                    ObjectOutputStream objlista = new ObjectOutputStream(listaout); 
                    objlista.writeObject(termi); 
                    objlista.flush();             // FIN SERIALIZACION TERMINO JUEGO CONFIGURADO
                }catch(IOException i){
                    System.out.println("IOException:" + i.getMessage());
                }finally{
                    try{
                        listaout.close();
                    }catch(IOException ex){
                        ex.printStackTrace();
                    }
                }
            }
        }
    }
    
    private class ManejaTerminoJuego implements EventHandler<Event>{             // MANEJADOR DE ENVENTO CLICL EN TABLE VIEW
        private TerminoAcademico term;
        
        public ManejaTerminoJuego(TerminoAcademico tr){
            term=tr;
        }

        @Override
        public void handle(Event t) {
             TerminoAcademico ta= (TerminoAcademico)losterminos.getSelectionModel().getSelectedItem();        }
        
    
    }
    
    public TerminoAcademico SetTerminoAcademico(TerminoAcademico teraca){             // DEFINE EL TERMINO JUEGO CONFIGURADO
        return this.teraca= teraca;
    }

   
}
