/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie;

import com.pooespol.appmillionarie.modelo.Materia;
import com.pooespol.appmillionarie.modelo.Pregunta;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author raini
 */
public class AdPreguntasController {             // ATRIBUTOS DE LA CLASE Y LA VENTANA
    private ArrayList<Materia> materias;
    private ArrayList<Pregunta> preguntas;
    @FXML
    private Button adTerminos;
    @FXML
    private Button adMatPar;
    @FXML
    private Button salir;
    @FXML
    private ComboBox cambiarMateria;
    @FXML
    private TableView laspreguntas;
    @FXML
    private TableColumn<Pregunta, String> colPregunta;
    @FXML
    private TableColumn<Pregunta, String> colNivel;
    @FXML
    private Button agPregunta;

    @FXML
    private void switchToAdTerminos() throws IOException {             // CAMBIA A TERMINOS
        App.setRoot("secondary");
    }

    @FXML
    private void switchToAdMaterias() throws IOException {             // CAMBIA A MATERIAS
        App.setRoot("adMaterias");
    }

    @FXML
    private void switchToMenu() throws IOException {             // CAMBIA A MENU
        App.setRoot("primary");
    }

    public AdPreguntasController() {             // CONTROLADOR Y CARGA DE MATERIAS Y PREGUNTAS
        Datos d1 = new Datos();
        materias = d1.cargarMaterias();
        preguntas = d1.cargarPreguntas();
    }

    @FXML
    private void mostrarPreguntas() {             // MUESTRA LAS PREGUNTAS SEGUN LA MATERIA ESCOGIDA
        ArrayList<Pregunta> pregun= new ArrayList<>();
        Materia mat = (Materia)cambiarMateria.getValue();
        for (Pregunta p: preguntas){
                if(p.getMateria().equals(mat)){
                    pregun.add(p);
                }
            }
        colPregunta.setCellValueFactory(new PropertyValueFactory<>("enunciado"));             // LLENA EL TABLE VIEW
        colNivel.setCellValueFactory(new PropertyValueFactory<>("nivel"));
        laspreguntas.getItems().setAll(pregun);
    }

    @FXML
    private void switchToAgPregunta() throws IOException {             // CAMBIA A AGREGAR PREGUNTA
        App.setRoot("agPregunta");
    }
    @FXML
    public void agregarImagenbtn(){             // AGREGA LAS IMAGENES DE LA VENTANA
        FileInputStream imageadd = null;
        try{
            imageadd = new FileInputStream(App.pathimagen+"add2cosa.png");
            ImageView imv = new ImageView(new Image(imageadd,50,50,false,false));
            agPregunta.setGraphic(imv);
        
        }catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
    }
    @FXML
    private void initialize() {             // ASIGNA VALORES AL COMBO BOX Y AGREGA IMAGEN
        cambiarMateria.getItems().setAll(materias);
        agregarImagenbtn();
                
    }
    
}
