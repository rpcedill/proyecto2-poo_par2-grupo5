/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie;

import com.pooespol.appmillionarie.modelo.Estudiante;
import com.pooespol.appmillionarie.modelo.Materia;
import com.pooespol.appmillionarie.modelo.Paralelo;
import com.pooespol.appmillionarie.modelo.TerminoAcademico;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author raini
 */
public class AdParalelosController {             //ATRIBUTOS DE LA VENTANA Y LAS CLASE
    private ArrayList<Materia> materias;
    private ArrayList<Paralelo> paralelos;
    private ArrayList<TerminoAcademico> terminosAc;
    private ArrayList<Estudiante> estudiantes;
    @FXML
    private Button adMatPar;
    @FXML
    private Button adPreguntas;
    @FXML
    private Button salir;
    @FXML
    private Button adMaterias;
    @FXML
    private TextField textoNumero;
    @FXML
    private TextField textoRuta;
    @FXML
    private Button agregarParalelo;
    @FXML
    private TableColumn<Paralelo, String> colmateria;
    @FXML
    private TableColumn<Paralelo, String> coltermino;
    @FXML
    private TableColumn<Paralelo, String> colnumero;
    @FXML
    private ComboBox cmbMaterias;
    @FXML
    private ComboBox cmbTerminos;
    @FXML
    private TableView losparalelos;
    

    @FXML
    private void switchToAdPreguntas() throws IOException {             // CAMBIA A PREGUNTAS
        App.setRoot("adPreguntas");
    }
    
    @FXML
    private void switchToAdTerminos() throws IOException {             // CAMBIA A TERMINOS
        App.setRoot("secondary");
    }
    
    @FXML
    private void switchToMenu() throws IOException {             // CAMBIA A MENU
        App.setRoot("primary");
    }

    @FXML
    private void switchToAdMaterias() throws IOException {             // CAMBIA A MATERIAS
        App.setRoot("adMaterias");
    }

    public AdParalelosController() {             // CONSTRUCTOR, CARGA DE MATERIAS, TERMINOS Y PARALELOS
        Datos d1 = new Datos();
        materias = d1.cargarMaterias();
        terminosAc = d1.cargarTerminos();
        paralelos = d1.cargarParalelos();
    }
    
    @FXML    
    public void agregarImagenbtn(){             // AGREGA LAS IMAGENES DE LA VENTANA
        FileInputStream imageadd = null;
        try{
            imageadd = new FileInputStream(App.pathimagen+"add_cosa.png");
            ImageView imv = new ImageView(new Image(imageadd,50,50,false,false));
            agregarParalelo.setGraphic(imv);
        
        }catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
    }
    
    @FXML
    private void initialize() {             // ASIGNA VALORES Y LLENA EL TABLE VIEW Y EL COMBOBOX Y AGREGA IMAGEN
        cmbMaterias.getItems().setAll(materias);
        cmbTerminos.getItems().setAll(terminosAc);
        colmateria.setCellValueFactory(new PropertyValueFactory<>("materia"));
        coltermino.setCellValueFactory(new PropertyValueFactory<>("termino"));
        colnumero.setCellValueFactory(new PropertyValueFactory<>("numero"));
        losparalelos.getItems().setAll(paralelos);
        agregarImagenbtn();
    }
    @FXML
    private void agregarParalelo() {             // FUNCION DE AGREGAR PARALELO
        System.out.println("Guardando Paralelo");             // OBTENCION DE LO INGRESADO
        String numero = textoNumero.getText();
        TerminoAcademico t = (TerminoAcademico)cmbTerminos.getValue();
        Materia m = (Materia)cmbMaterias.getValue();
        if(numero.isEmpty()|| t==null || m==null ){             // VALIDACIONES DE NULO O EXISTENTE
            Alert vacio= new Alert(Alert.AlertType.INFORMATION);
            vacio.setTitle("Informacion de campo no ingresado");             // ALERTA ERROR
            vacio.setHeaderText("Campo sin llenar");
            vacio.setContentText("Llene todos los campos de registro");
            vacio.showAndWait();
        //}
        }else if(paraleloExiste(m, t,Integer.valueOf (numero))){             // COMPRUEBA SI EL PARALELO EXISTE
            System.out.println("La materia ingresada ya existe");             // ALERTA ERROR
            Alert alertIngreso = new Alert(Alert.AlertType.INFORMATION);
            alertIngreso.setTitle("Informacion de ingreso de materia");
            alertIngreso.setHeaderText("Ingresó informacion ya existente");
            alertIngreso.setContentText("Materia ingresada ya existe\nINGRESE UNO NUEVO");
            alertIngreso.showAndWait();          
        }else{
            int x = materias.indexOf(m);             // CREA EL PARALELO
            int y = terminosAc.indexOf(t);
            Paralelo par= new Paralelo(materias.get(x),terminosAc.get(y), Integer.valueOf (numero));
            paralelos.add(par);
            ArrayList<Paralelo> para= terminosAc.get(y).getParalelos();
            para.add(par);
            terminosAc.get(y).setParalelos(para);             // ASIGNA EL PARALELO DENTRO DEL TERMINO
            if (textoRuta.getText().isEmpty()){
                System.out.print("No se añadieron estudiantes");             // NO SE INCRESO CSV
            }else{
                agregarEstudiantes(par);
                    }
            FileOutputStream listaout= null;             // INICIO DE SERIALIZACION TERMINOS NUEVOS
                try{ 
                    listaout = new FileOutputStream(App.pathTerminosAc);            
                    ObjectOutputStream objlista = new ObjectOutputStream(listaout); 
                    objlista.writeObject(terminosAc); 
                    objlista.flush();             // FIN DE SERIALIZACION DE TERMINOS NUEVOS
                }catch(IOException i){
                    System.out.println("IOException:" + i.getMessage());
                }finally{
                    try{
                        listaout.close();
                    }catch(IOException ex){
                        ex.printStackTrace();
                    }
                }
            FileOutputStream listaout2= null;             // INICIO SERIALIZACION PARALELOS
            try{
            listaout2= new FileOutputStream(App.pathParalelos);
            ObjectOutputStream objlista2 = new ObjectOutputStream(listaout2); 
            objlista2.writeObject(paralelos); 
            objlista2.flush();             // FIN SERIALIZACION PARALELOS

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("INFORMACION");
                alert.setHeaderText("Resultado de la operacion");             // ALERTA EXITO
                alert.setContentText("Nuevo paralelo agregado exitosamente");
                alert.showAndWait();
                App.setRoot("adParalelos");             // REFRESCAR VENTANA
            }catch(IOException ex){
                ex.printStackTrace();
                System.out.println("IOException:" + ex.getMessage());
            }finally{
                try{
                    listaout2.close();
                }catch(IOException ex){
                    ex.printStackTrace();
                }
            }
        }
        
    }
    
    private void agregarEstudiantes(Paralelo p){             // FUNCION DE AGREGAR ESTUDIANTES CSV
        estudiantes =new ArrayList<>();
        BufferedReader br = null;             // ABRE EL CSV
            try {
                br =new BufferedReader(new FileReader("src/main/resources/com/pooespol/appmillionarie/files/"+textoRuta.getText()));
                String linea = br.readLine();
                linea = br.readLine();
                while (null!=linea) {
                    String [] datos = linea.split(";");             // ASIGANA ATRIBUTOS  DE ESTUDIANTE
                    estudiantes.add(new Estudiante(datos[0],datos[1],datos[2]));
                    linea = br.readLine();
                }
            } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
            ex.printStackTrace();
        }
        p.setEstudiantes(estudiantes);
        System.out.print("Estudiantes Guardados");
        FileOutputStream listaout3= null;             // INCIO SERIALIZACION DE PARALELOS CON ESTUDIANTES
        try {
            listaout3= new FileOutputStream(App.pathParalelos);
            ObjectOutputStream objlista3 = new ObjectOutputStream(listaout3); 
            objlista3.writeObject(paralelos); 
            objlista3.flush();             // FIN DE SERIALIZACION DE PARALELOS CON ESTUDIANTES
        }catch(IOException ex){
                ex.printStackTrace();
                System.out.println("IOException:" + ex.getMessage());
            }finally{
                try{
                    listaout3.close();
                }catch(IOException ex){
                    ex.printStackTrace();
                }
            }
        FileOutputStream listaout4= null;             // INCIO SERIALIZACION DE ESTUDIANTES
        try {
            listaout4= new FileOutputStream(App.pathEstudiantes);
            ObjectOutputStream objlista4 = new ObjectOutputStream(listaout4); 
            objlista4.writeObject(estudiantes); 
            objlista4.flush();             // FIN DE SERIALIZACION DE ESTUDIANTES
        }catch(IOException ex){
                ex.printStackTrace();
                System.out.println("IOException:" + ex.getMessage());
            }finally{
                try{
                    listaout4.close();
                }catch(IOException ex){
                    ex.printStackTrace();
                }
            }
            
            
    }
    public boolean paraleloExiste(Materia m, TerminoAcademico t, int numero){             // COMPRUEBA SI EXISTE EL PARALELO
        for (Paralelo p: paralelos){
            if (p.getMateria().equals(m) && p.getTermino().equals(t) && p.getNumero()==(numero)){
                return true;
            }
        }
        return false;
    }
    
}
