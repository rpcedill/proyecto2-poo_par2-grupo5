/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

/**
 *
 * @author raini
 */
public class ConfiguracionController {             // ATRIBUTOS DE LA VENTANA Y LA CLASE 

    @FXML
    private Button adTerminos;
    @FXML
    private Button adMatPar;
    @FXML
    private Button adPreguntas;
    @FXML
    private ImageView imgConfig;
    @FXML
    private Button salir;

    @FXML
    private void switchToAdTerminos() throws IOException {             // CAMBIA A TERMINOS
        App.setRoot("secondary");
    }

    @FXML
    private void switchToMatPar() throws IOException {             // CAMBIA A MATERIAS Y PARALELOS
        App.setRoot("adMaterias");
    }

    @FXML
    private void switchToAdPreguntas() throws IOException {             // CAMBIA A PREGUNTAS
        App.setRoot("adPreguntas");
    }

    @FXML
    private void switchToMenu() throws IOException {             // CAMBIA A MENU
        App.setRoot("primary");
    }
    
}
