package com.pooespol.appmillionarie;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import static javafx.application.Application.launch;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;             // RUTAS DE ARCHIVOS SERIALIZADOS
    public static String pathTerminosAc="src/main/resources/com/pooespol/appmillionarie/files/terminos";
    public static String pathimagen= "src/main/resources/com/pooespol/appmillionarie/files/";
    public static String pathMaterias="src/main/resources/com/pooespol/appmillionarie/files/materias";
    public static String pathParalelos="src/main/resources/com/pooespol/appmillionarie/files/paralelos";
    public static String pathPreguntas="src/main/resources/com/pooespol/appmillionarie/files/preguntas";
    public static String pathEstudiantes="src/main/resources/com/pooespol/appmillionarie/files/estudiantes";
    public static String pathTerminoJuego="src/main/resources/com/pooespol/appmillionarie/files/terminoJuego";
    @Override
    public void start(Stage stage) throws IOException {             // CARGA LA VENTANA CON UN TAMAÑO GRNADE POR DEFECTO
        scene = new Scene(loadFXML("primary"), 750, 550);
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }
    
    static void cambiaRoot (Parent rootnode){             // RECIBE LOS DATOS PARA LA VENTANA JUEGO
        scene.setRoot(rootnode);
    }
    

}
