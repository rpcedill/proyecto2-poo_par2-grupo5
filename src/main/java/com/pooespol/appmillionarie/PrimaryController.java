package com.pooespol.appmillionarie;

import com.pooespol.appmillionarie.modelo.TerminoAcademico;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;


public class PrimaryController {             // ATRIBUTOS DE LA CLASE Y DE LA VENTANA

    @FXML
    Button primaryButton;
    @FXML
    Button nuevogame;
    @FXML
    Button salir;
    
    @FXML
    private void switchToSecondary() throws IOException {             // CAMBIA A CONFIGURACION
        App.setRoot("configuracion");
    }
    @FXML
    private void switchToNuevoJuego() throws IOException {             // CAMBIA A NUEVO JUEGO
        App.setRoot("nuevoJuego");
    }
    @FXML 
    private void initialize(){             // CARGA IMAGENES
        agregarImagenbtn();
    }
    @FXML
    public void agregarImagenbtn(){             // INSERTA IMAGENES
        
        FileInputStream imageconf=null;
        FileInputStream imagenew=null;
        FileInputStream imageout= null;
        try{
            
            imageconf= new FileInputStream(App.pathimagen+"configurar14.png");             // BUSCA LAS IMAGENES Y LAS COLOCA
            imagenew= new FileInputStream(App.pathimagen+"game.png");
            imageout = new FileInputStream(App.pathimagen+"salirbtn.png");
            Image imagenconfig = new Image(imageconf,62,62,false,false );
            ImageView imv = new ImageView(imagenconfig);
            primaryButton.setGraphic(imv);
            nuevogame.setGraphic(new ImageView(new Image(imagenew,56,56,false,false)));
            salir.setGraphic(new ImageView(new Image(imageout,56,56,false,false)));
        
        }catch(FileNotFoundException ex){
            ex.printStackTrace();
        }finally{
            try{
            
            imageconf.close();
            }catch(IOException ex){
                ex.printStackTrace();
            }
        
            }
    }
    
    
    


}
