/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie;

import com.pooespol.appmillionarie.modelo.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author raini
 */
public class Datos {             // ATRIBUTOS QUE SON LAS LISTAS VACIAS O SERIALIZADAS
    private ArrayList<Paralelo> paralelos;
    private ArrayList<TerminoAcademico>terminosAc ;
    private ArrayList<Pregunta> preguntas;
    private ArrayList<Materia> materias;
    private ArrayList<Estudiante> estudiantes;
    private ArrayList<TerminoAcademico> termino;

    public Datos() {             // CONSTRUCTOR
        this.paralelos = new ArrayList<>();
        this.terminosAc = new ArrayList<>();
        this.preguntas = new ArrayList<>();
        this.materias = new ArrayList<>();
        this.estudiantes = new ArrayList<>();
                
    }
    public ArrayList<Pregunta> cargarPreguntas(){             // CARGA LAS PREGUNTAS SERIALIZADAS
        preguntas = new ArrayList<>();
        
        FileInputStream entropreguntas = null;
        try{
            entropreguntas= new FileInputStream(App.pathPreguntas);
            ObjectInputStream pg = new ObjectInputStream(entropreguntas);
            preguntas= (ArrayList<Pregunta>)pg.readObject();
        }catch (FileNotFoundException ex) {
            System.out.println("archivo no existe");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (entropreguntas != null) {
                    entropreguntas.close();
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return preguntas;             // DEVUELVE LAS PREGUNTAS
    }
    public ArrayList<Materia> cargarMaterias(){             // CARGA LAS MATERIAS SERIALIZADAS
        materias = new ArrayList<>();
        FileInputStream entromaterias = null;
        try{
            entromaterias= new FileInputStream(App.pathMaterias);
            ObjectInputStream mat = new ObjectInputStream(entromaterias);
            materias= (ArrayList<Materia>)mat.readObject();
        }catch (FileNotFoundException ex) {
            System.out.println("archivo no existe");
        }   catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (entromaterias != null) {
                    entromaterias.close();
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return materias;             // DEVUELVE LAS MATERIAS
    }
    public ArrayList<Estudiante> cargarEstudiantes(){             // CARGA LOS ESTUDIANTES SERIALIZADOS
        estudiantes = new ArrayList<>();
        
        FileInputStream entroestu = null;
        try{
            entroestu= new FileInputStream(App.pathEstudiantes);
            ObjectInputStream es = new ObjectInputStream(entroestu);
            estudiantes= (ArrayList<Estudiante>)es.readObject();
        }catch (FileNotFoundException ex) {
            System.out.println("archivo no existe");
        }   catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (entroestu != null) {
                    entroestu.close();
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return estudiantes;             // DEVUELVE LOS ESTUDIANTES
    }
    public ArrayList<TerminoAcademico> cargarTerminos(){             // CARGA LOS TERMINOS SERIALIZADOS
        terminosAc = new ArrayList<>();
        FileInputStream entroaterminos = null;
        try{
            entroaterminos= new FileInputStream(App.pathTerminosAc);
            ObjectInputStream term = new ObjectInputStream(entroaterminos);
            terminosAc= (ArrayList<TerminoAcademico>)term.readObject();
        }catch (FileNotFoundException ex) {
            System.out.println("archivo no existe");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (entroaterminos != null) {
                    entroaterminos.close();
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return terminosAc;             // DEVUELVE LOS TERMINOS 
    }
    
    public ArrayList<Paralelo> cargarParalelos(){             // CARGA LOS PARALELOS SERIALIZADOS
        paralelos = new ArrayList<>();
        
        FileInputStream entroparalelos = null;
        try{
            entroparalelos= new FileInputStream(App.pathParalelos);
            ObjectInputStream par = new ObjectInputStream(entroparalelos);
            paralelos= (ArrayList<Paralelo>)par.readObject();
        }catch (FileNotFoundException ex) {
            System.out.println("archivo no existe");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (entroparalelos != null) {
                    entroparalelos.close();
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return paralelos;             // DEVUELVE LOS PARALELOS
    }
    
    public TerminoAcademico cargarTerminoJuego(){             // CARGAR EL TERMINO CONFIGURADO
        termino = new ArrayList<>();
        
        FileInputStream ter = null;
        try{
            ter= new FileInputStream(App.pathTerminoJuego);
            ObjectInputStream terjuego = new ObjectInputStream(ter);
            termino= (ArrayList<TerminoAcademico>)terjuego.readObject();
        }catch (FileNotFoundException ex) {
            System.out.println("archivo no existe");
            try {
                ter.close();
                return null;
            } catch (IOException ex1) {
                ex1.printStackTrace();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (ter != null) {
                    ter.close();
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return termino.get(0);             // DECUELVE EL TERMINO CONFIGURADO
    }
}
