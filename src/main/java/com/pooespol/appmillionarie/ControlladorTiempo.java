/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie;

import com.pooespol.appmillionarie.modelo.Estudiante;
import com.pooespol.appmillionarie.modelo.Pregunta;
import com.pooespol.appmillionarie.modelo.PreguntaEnJuego;
import com.pooespol.appmillionarie.modelo.TipoComodin;
import com.pooespol.appmillonarie.hilos.CambiaOpciones;
import com.pooespol.appmillonarie.hilos.Temporizador;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

/**
 *
 * @author Gustavo
 */

public class ControlladorTiempo {             // ATRIBUTOS DE LA VENTANA Y CLASE
    @FXML
    Label txCuenta;
    @FXML
    Button btnstart;
    @FXML
    Button r1;
    @FXML
    Button r2;
    @FXML
    Button r3;
    @FXML
    Button r4;
    @FXML
    Label pregunta;
    @FXML
    GridPane gridrespuestas;
    @FXML
    ImageView imagenaviso;
    @FXML
    Button btnsalirjuego;
    @FXML
    private ImageView cincuenta;
    @FXML
    private ImageView salon;
    @FXML
    private ImageView compañero;
    private ArrayList<TipoComodin> comodines;
    @FXML
    TextFlow flowtexto;
    private ArrayList<Pregunta> preguntasJuego;
    private ArrayList<PreguntaEnJuego> preguntas1;
    private ArrayList<PreguntaEnJuego> preguntas2;
    private ArrayList<PreguntaEnJuego> preguntas3;
    private ArrayList<TipoComodin> comodinesUsados;
    private int sigPregunta=0;
    private int numeroPreguntas;
    private Estudiante participante;
    private Estudiante compañeroParti;
    
    
    Temporizador temp;
    CambiaOpciones co;
    
    public ControlladorTiempo(){             // CONSTRUCTOR Y CARGA DE COMODINES
        comodines= new ArrayList<>();
        comodines.add(TipoComodin.COMPAÑERO);
        comodines.add(TipoComodin.C50_50);
        comodines.add(TipoComodin.SALON);
        comodinesUsados = new ArrayList<>();   
        
        
    }
    
    public void comenzarCuenta(){             // SE ESTABLECEN LAS PREGUNTAS SEGUN SI NIVEL
        Pregunta pregunt =new Pregunta();
        preguntas1 = new ArrayList<>();
        preguntas2 = new ArrayList<>();
        preguntas3 = new ArrayList<>();
        for (Pregunta p: preguntasJuego){
            switch (p.getNivel()) {
                case 1:
                    preguntas1.add(new PreguntaEnJuego(p,false));
                    break;
                case 2:
                    preguntas2.add(new PreguntaEnJuego(p,false));
                    break;
                case 3:
                    preguntas3.add(new PreguntaEnJuego(p,false));
                    break;
                default:
                    break;
            }
            
        }Random rt = new Random();             // SE SELECCIONA LA PREGUNTA AL AZAR SEGUN EL NIVEL Y EL NUMERO DE PREGUNTA
                    if ((numeroPreguntas/3)>=sigPregunta+1){
                        int n = rt.nextInt(preguntas1.size());
                        PreguntaEnJuego preguntaA = preguntas1.get(n);
                        pregunt= preguntaA.getPregunta();
                        preguntasJuego.remove(pregunt);             // SE QUITA LA PREGUNTA QUE SE SELECCIONA PARA EVITAR REPETICION
                    }
                    else if(((numeroPreguntas*2)/3)>=sigPregunta+1){
                        int n = rt.nextInt(preguntas2.size());
                        PreguntaEnJuego preguntaA = preguntas2.get(n);
                        pregunt= preguntaA.getPregunta();
                        preguntasJuego.remove(preguntaA);// SE QUITA LA PREGUNTA QUE SE SELECCIONA PARA EVITAR REPETICION
                    }
                    else if(numeroPreguntas>=sigPregunta+1){
                        int n = rt.nextInt(preguntas3.size());
                        PreguntaEnJuego preguntaA = preguntas3.get(n);
                        pregunt= preguntaA.getPregunta();
                        preguntasJuego.remove(preguntaA);// SE QUITA LA PREGUNTA QUE SE SELECCIONA PARA EVITAR REPETICION
                    }
        temp=new Temporizador(txCuenta,imagenaviso,btnsalirjuego,btnstart,r1,r2,r3,r4); 
        co= new CambiaOpciones(pregunt,pregunta,gridrespuestas,sigPregunta,             // COLOCA LOS BOTONES Y LA PREGUNTA
                btnstart,flowtexto,numeroPreguntas,r1,r2,r3,r4,sigPregunta);
        Thread hilo = new Thread(temp);            // CREA EL RELOJ
        hilo.setDaemon(true);
        hilo.start();            // INICIA EL RELOJ
        btnstart.setDisable(true);
        btnsalirjuego.setDisable(true);
        
        Thread hilogrid= new Thread(co);            
        hilogrid.setDaemon(true);
        hilogrid.start();            // COLOCA LOS BOTONES Y LA PREGUNTA
        
        
    }
    @FXML
    private void salirpreguntas()throws IOException{             // CAMBIA A MENU
        btnstart.setDisable(false);
        App.setRoot("primary");
    }
    

    public void setPreguntasJuego(ArrayList<Pregunta> preguntasJuego) {             // SE SETEAN LAS PREGUNTAS
        this.preguntasJuego = preguntasJuego;
    }

    public void setParticipante(Estudiante participante) {             // SE SETEA EL PARTICIPANTE
        this.participante = participante;
    }

    public void setCompañeroParti(Estudiante compañeroParti) {             // SE SETEA EL COMPAÑERO
        this.compañeroParti = compañeroParti;
    }

    public void setNumeroPreguntas(int numeroPreguntas) {             // SE SETEA EL TOTAL DE PREGUNTAS
        this.numeroPreguntas = numeroPreguntas;
    }
    public void orden(){             // SE PONEN LOS BOTONES DE UN MISMO COLOR
        r1.setStyle("-fx-background-color: #ce1ced;-fx-border-color: #f0ff71;");
        r2.setStyle("-fx-background-color: #ce1ced;-fx-border-color: #f0ff71;");
        r3.setStyle("-fx-background-color: #ce1ced;-fx-border-color: #f0ff71;");
        r4.setStyle("-fx-background-color: #ce1ced;-fx-border-color: #f0ff71;");
        btnstart.setOnMouseClicked(new ManejaStart());             // EVENTO
        
    }
    
    private class ManejaStart implements EventHandler<Event>{             // MANEJA EVENTO STRAT
        
        public ManejaStart(){
            comenzarCuenta();             // MANDA A ELEGIR LA PREGUNTA INICIAR EL RELOJ U COLOCAR BOTONES
            sigPregunta+=1;
            cincuenta.setOnMouseClicked(new ManejadorEvento());             // EVNETOS DE COMODINES
            salon.setOnMouseClicked(new ManejadorEvento2());
            compañero.setOnMouseClicked(new ManejadorEvento3());
        }
       
        @Override
        public void handle(Event event){
            
            
        }
    }
    
    private class ManejadorEvento implements EventHandler<Event>{             // EVENTO COMODIN 50/50
        public ManejadorEvento(){
        }
        @Override
        public void handle(Event event){
            ArrayList<Button> botones = new ArrayList<>();             // LISTA DE BOTONES
            botones.add(r1);            botones.add(r2);
            botones.add(r3);            botones.add(r4);
            Button b = respuesta();             // SE OBTIENE EL BOTON CON RESPUESTA CORRECTA
            botones.remove(botones.indexOf(b));
            for (int i = 0; i<2;i++){
                botones.get(i).setDisable(true);             // SE DESHABILITAN 2 BOTONES CON RESPUESTA INCORRECTA
            }
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("COMODIN");             // ALERTA COMODIN
                            alert.setContentText("SE HAN QUITADO 2 REPUESTAS INCORRECTAS");
                            alert.setHeaderText("50/50");
                            alert.show();
                            comodines.remove(TipoComodin.C50_50);
                            comodinesUsados.add(TipoComodin.C50_50);             // AÑADE COMODIN USADO
                            cincuenta.setVisible(false);             // QUITA IMAGEN DEL COMODIN
                            
            });            
        }
    }
    private class ManejadorEvento2 implements EventHandler<Event>{             // EVENTO COMODIN SALON
        public ManejadorEvento2(){
        }
        @Override
        public void handle(Event event){
            Random rt = new Random();             // NUMEROS RANDOM DE PROBABILIDADES
            double numero = rt.nextInt(5)+50;
            double numero2 = (100-numero)/2;
            double numero3 = numero2/2; 
            Button b = respuesta();             // BOTON CON RESPUESTA CORRECTA
            ArrayList<Button> botones = new ArrayList<>();
            botones.add(r1);            botones.add(r2);
            botones.add(r3);            botones.add(r4);
            botones.remove(botones.indexOf(b));
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("COMODIN");             // ALERTA COMODIN SALON
                            alert.setContentText("PORCENTAJES DADOS POR EL SALON: \n"             // ASIGNA PROBABILIDADES A LAS RESPUESTAS A,B,C,D
                                    + botones.get(0).getText().charAt(0)+": "+String.valueOf(numero3)+"%\n"
                                    +b.getText().charAt(0)+": "+String.valueOf(numero)+"%\n"
                                    +botones.get(1).getText().charAt(0)+": "+String.valueOf(numero3)+"%\n"
                                    +botones.get(2).getText().charAt(0)+": "+String.valueOf(numero2)+"%\n");
                            alert.setHeaderText("SALON");
                            comodines.remove(TipoComodin.SALON);
                            comodinesUsados.add(TipoComodin.SALON);             // AÑADE COMODIN USADO
                            alert.setHeight(300);
                            alert.show();
                            salon.setVisible(false);             // QUITA IMAGEN COMODIN
            });            
        }
    }
    
    private Button respuesta(){             // METODO PARA OBETNER EL BOTON CON RESPUESTA CORRECTA
        Button boton = new Button();
        ArrayList<Button> botones = new ArrayList<>();             //LISTA DE BOTONES
        botones.add(r1);
        botones.add(r2);
        botones.add(r3);
        botones.add(r4);
        String preg = pregunta.getText();             // PREGUNTA
        for (Pregunta p: preguntasJuego){
            if (p.getEnunciado().equals(preg))
                for (Button b: botones){
            String respuesta = b.getText();
            if (respuesta.equals("A "+p.getRespuestaCorrecta())             // VEMOS LA RESPUESTA CORRECTA Y LA COMPARAMOS
                        ||respuesta.equals("B "+p.getRespuestaCorrecta())
                        ||respuesta.equals("C "+p.getRespuestaCorrecta())
                        ||respuesta.equals("D "+p.getRespuestaCorrecta())){
                        boton = b;             // ASIGNAMOS BOTON GANADOR
            }
        }
        }
        return boton;             // RETORNAMOS EL BOTON
    }
   
    
    private class ManejadorEvento3 implements EventHandler<Event>{             // EVENTO COMODIN COMPAÑERO
        public ManejadorEvento3(){
        }
        @Override
        public void handle(Event event){
            Random rt = new Random();             // NUMERO RANDOM DE SEGURIDAD
            int numero = rt.nextInt(15)+75;
            Button b = respuesta();             // BOTON CON RESPUESTA CORRECTA
            String literal = b.getText();
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("COMODIN");             // ALERTA COMODIN COMPAÑERO
                            alert.setContentText("ESTA "+String.valueOf(numero)+"% SEGURO QUE ES LA "+literal.charAt(0));
                            alert.setHeaderText("COMPAÑERO "+compañeroParti.getNombre());             // MUESTRA NOMBRE DEL COMPAÑERO Y PROCENTAJE DE SEGURIDAD DE LA RESPUESTA
                            alert.show();
                            comodines.remove(TipoComodin.COMPAÑERO);
                            comodinesUsados.add(TipoComodin.COMPAÑERO);             // AÑADE COMODIN USADO
                            alert.setHeight(200);
                            compañero.setVisible(false);             // QUITA IMAGEN COMODIN
            });            
        }
    }
    
    @FXML
    public void bloquear(Button a1,Button a2,Button a3){             // BLOQUEA BOTONES QUE NO SE ELIGIERON
        a1.setDisable(true);
        a2.setDisable(true);
        a3.setDisable(true);
        btnstart.setDisable(false);
    }
    @FXML
    private void comprobarRespuesta(String respuesta,Button b){             // COMPRUEBA LA RESPUESTA DEL BOTON PRESIONADO
        temp.setProgresion(false);
        co.setProgresion(false);
        String preg = pregunta.getText();
        for (Pregunta p: preguntasJuego){
            if (p.getEnunciado().equals(preg)){
                if(respuesta.equals("A "+p.getRespuestaCorrecta())             // VALIDACIONES
                        ||respuesta.equals("B "+p.getRespuestaCorrecta())
                        ||respuesta.equals("C "+p.getRespuestaCorrecta())
                        ||respuesta.equals("D "+p.getRespuestaCorrecta())){
                    b.setStyle("-fx-border-color:cyan;-fx-background-color:green;");             // COLOR VERDE, BOTON CORRECTO
                    if(sigPregunta==numeroPreguntas){             // SI ACIERTA TODAS LAS PREGUNTAS
                        btnstart.setDisable(true);             // DESABILITA EL START
                        juegoGanado();             // JUEGO GANADO 
                    }
                    
                }else{
                    System.out.println("respuesta incorrecta");
                    b.setStyle("-fx-border-color:cyan;-fx-background-color:red;");             // COLOR ROJO, BOTON INCORRECTO
                    juegoPerdido();
                    btnstart.setDisable(true);             // DESABILITA EL STRAT
                }
            }
        }

    }
    @FXML
    private void juegoPerdido(){             // MUESTRA LOS RESULTADOS DEL JEUGO PERDIDO
        int niveles = (sigPregunta-1)/(numeroPreguntas/3);
        
        Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);             // ALERTA JUEGO PERDIDO, PUNTAJE, NIVEL, COMODINES
                            alert.setTitle("JUEGO TERMINADO");
                            alert.setHeight(200);
                            alert.setContentText("RESPUESTA INCORRECTA"+"\n"
                                    + "COMODINES USADOS: "+comodinesUsados+"\n"
                                    + "NIVELES SUPERADOS: "+niveles);
                            alert.setHeaderText("PUNTAJE: "+(sigPregunta-1)+"\n"
                                    + "NIVELES ");
                            alert.show();
                                                     
            });
    }
    
    @FXML
    private void juegoGanado(){             // RESULTADOS JUEGO GANADO
        Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("JUEGO TERMINADO");             // ALERTA JUEGO GANADO, NONMBRE, COMODINES Y NIVEL Y PUNTAJE
                            alert.setContentText("HAS GANADO "+participante.getNombre()+"\n"
                                    + "COMODINES USADOS: "+comodinesUsados);
                            alert.setHeaderText("PUNTAJE: "+(sigPregunta));
                            alert.setHeight(200);
                            alert.show();
                                                     
            });
    }
    
    @FXML
    public void resultadoPregunta(){             // VALIDA RESULTADOS DEL BOTON R1
        bloquear(r2,r3,r4);
        String respuesta = r1.getText();
        comprobarRespuesta(respuesta,r1);
        btnsalirjuego.setDisable(false);
    }
    
    public void resultadoPregunta2(){             // VALIDA RESULTADOS DEL BOTON R2
        bloquear(r1,r3,r4);
        String respuesta = r2.getText();
        comprobarRespuesta(respuesta,r2);
        btnsalirjuego.setDisable(false);
    }
   
    public void resultadoPregunta3(){             // VALIDA RESULTADOS DEL BOTON R3
        bloquear(r1,r2,r4);
        String respuesta = r3.getText();
        comprobarRespuesta(respuesta,r3);
        btnsalirjuego.setDisable(false);
    }
   
    public void resultadoPregunta4(){             // VALIDA RESULTADOS DEL BOTON R4
        bloquear(r1,r3,r2);
        String respuesta = r4.getText();
        comprobarRespuesta(respuesta,r4);
        btnsalirjuego.setDisable(false);
    }

    
}
