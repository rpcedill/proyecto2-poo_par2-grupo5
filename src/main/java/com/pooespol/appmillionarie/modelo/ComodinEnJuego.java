/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie.modelo;

/**
 *
 * @author raini
 */
public class ComodinEnJuego {               // ATRIBUTOS DE LA CLASE
    private TipoComodin comodin;
    private boolean usado;

    public ComodinEnJuego(TipoComodin comodin, boolean usado) {               // CONSTRUCTOR
        this.comodin = comodin;
        this.usado = usado;
    }
               // SETTER Y GETTERS
    public TipoComodin getComodin() {
        return comodin;
    }

    public boolean getUsado() {
        return usado;
    }

    public void setUsado(boolean usado) {
        this.usado = usado;
    }
    
    
}
