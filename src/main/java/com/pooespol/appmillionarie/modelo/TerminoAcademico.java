/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie.modelo;

import com.pooespol.appmillionarie.modelo.Paralelo;
import java.util.Objects;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Gustavo
 */
public class TerminoAcademico implements Serializable  {               // ATRIBUTOS DE LA CLASE
    private static final long serialVersionUID = 2778396492389753943L;
    private String year;
    private int numeroTermino;
    private ArrayList<Paralelo> paralelos;

    public TerminoAcademico(String año, int numero) {               // CONSTRUCTOR
        this.year = año;
        this.numeroTermino = numero;
        this.paralelos= new ArrayList<>();
    }

    public TerminoAcademico(String año, int numero, ArrayList<Paralelo> paralelos) {               // CONSTRUCTOR
        this.year = año;
        this.numeroTermino = numero;
        this.paralelos = paralelos;
    }
                 // SETTERS GETTERS TOSTRING EQUALS  
    public String getYear(){
        return this.year;
    }
    public int getNumeroTermino(){
        return this.numeroTermino;
    }

    @Override
    public String toString(){
        return year+"-"+numeroTermino;
    }

    public ArrayList<Paralelo> getParalelos() {
        return paralelos;
    }

    public void setParalelos(ArrayList<Paralelo> paralelos) {
        this.paralelos = paralelos;
    }
    

    public void setYear(String year) {
        this.year = year;
    }

    public void setNumeroTermino(int numeroTermino) {
        this.numeroTermino = numeroTermino;
    }
    
    public String toSting(){
        return "Termino Configurado: "+year+"-"+numeroTermino;

    }
    
    public String escribirLinea() {
        return  year + ";" + numeroTermino ;
    }

    /*public int compareTo(TerminoAcademico o) {
        
        return year.compareToIgnoreCase(o.year);
    }
    */
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.year);
        return hash;
    }
    
    /*@Override
    public boolean equals(Object obj){
        if(this==obj){
        return true;
        }
        if(obj!=null && getClass()==obj.getClass()){
            TerminoAcademico other = (TerminoAcademico)obj;
            return year.equalsIgnoreCase(other.year);
        }
        if(obj!=null && getClass()==obj.getClass()){
            TerminoAcademico other = (TerminoAcademico)obj;
            return numeroTermino.equalsIgnoreCase(other.numeroTermino);
        }
        return false;
    }
    */
    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TerminoAcademico other = (TerminoAcademico) obj;
        if (!Objects.equals(this.year, other.year)) {
            return false;
        }
        if (!Objects.equals(this.numeroTermino, other.numeroTermino)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
