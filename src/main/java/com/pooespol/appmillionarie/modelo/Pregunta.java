/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie.modelo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author raini
 */
public class Pregunta implements Serializable{               // ATRIBUTOS DE LA CLASE
    private static final long serialVersionUID = -7789494534959859759L;
    private String enunciado;
    private int nivel;
    private ArrayList<String> posiblesRespuestas;
    private String respuestaCorrecta;
    private Materia materia;

    public Pregunta(String enunciado) {               // CONSTRUCTOR
        this.enunciado = enunciado;
    }

    public Pregunta(Materia materia) {               // CONSTRUCTOR
        this.materia = materia;
    }

    public Pregunta(String enunciado, int nivel, ArrayList<String> posiblesRespuestas, String respuestaCorrecta, Materia materia) {               // CONSTRUCTOR
        this.enunciado = enunciado;
        this.nivel = nivel;
        this.posiblesRespuestas = posiblesRespuestas;
        this.respuestaCorrecta = respuestaCorrecta;
        this.materia = materia;
    }

    public Pregunta() {               // CONSTRUCTOR
    }
               // SETTERS GETTERS TOSTRING EQUALS
    public String getEnunciado() {
        return enunciado;
    }

    public int getNivel() {
        return nivel;
    }

    public ArrayList<String> getPosiblesRespuestas() {
        return posiblesRespuestas;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public Materia getMateria() {
        return materia;
    }

    @Override
    public String toString() {
        return enunciado + ", " + nivel + " {" + materia + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.enunciado);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pregunta other = (Pregunta) obj;
        return true;
    }
    
}
