/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie.modelo;

import java.util.Objects;

/**
 *
 * @author raini
 */
public class PreguntaEnJuego {               // ATRIBUTOS DE LA CLASE
    private Pregunta pregunta;
    private boolean contestada;

    public PreguntaEnJuego(Pregunta pregunta, boolean contestada) {               // CONSTRUCTOR
        this.pregunta = pregunta;
        this.contestada = contestada;
    }
               // SETTERS GETTERS Y EQUALS
    public Pregunta getPregunta() {
        return pregunta;
    }

    public boolean getContestada() {
        return contestada;
    }

    public void setContestada(boolean contestada) {
        this.contestada = contestada;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PreguntaEnJuego other = (PreguntaEnJuego) obj;
        if (this.contestada != other.contestada) {
            return false;
        }
        if (!Objects.equals(this.pregunta, other.pregunta)) {
            return false;
        }
        return true;
    }
    
    
    
}
