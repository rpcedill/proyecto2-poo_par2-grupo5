/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie.modelo;
import com.pooespol.appmillionarie.modelo.Estudiante;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author raini
 */
public class Paralelo implements Serializable{               // ATRIBUTOS DE LA CLASE
    private static final long serialVersionUID = -7358494534959859759L;
    private Materia materia;
    private TerminoAcademico termino;
    private int numero;
    private ArrayList<Estudiante> estudiantes;

    public Paralelo(Materia materia, TerminoAcademico termino, int numero, ArrayList<Estudiante> estudiantes) {               // CONSTRUCTOR
        this.materia = materia;
        this.termino = termino;
        this.numero = numero;
        this.estudiantes = estudiantes;
    }

    public Paralelo(Materia materia, TerminoAcademico termino, int numero) {               // CONSTRUCTOR
        this.materia = materia;
        this.termino = termino;
        this.numero = numero;
        this.estudiantes = new ArrayList<>();
    }
               // SETTERS GETTERS TOSTRING 
    public Materia getMateria() {
        return materia;
    }

    public TerminoAcademico getTermino() {
        return termino;
    }

    public int getNumero() {
        return numero;
    }

    public ArrayList<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public void setEstudiantes(ArrayList<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
    }
    

    @Override
    public String toString() {
        return materia + ", " + numero;
    }
    

    
    
}
