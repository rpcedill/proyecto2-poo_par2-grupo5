/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie.modelo;
import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author raini
 */
public class Juego {               // ATRIBUTOS DE LA CLASE
    private Paralelo paralelo;
    private Date fecha;
    private int preguntasPorNivel;
    private int nivelAlcanzado;
    private String Premio;
    private Estudiante participante;
    private Estudiante compañero;
    private ArrayList<ComodinEnJuego> comodines;
    private ArrayList<PreguntaEnJuego> preguntas;

    public Juego(Paralelo paralelo, Date fecha, int preguntasPorNivel, int nivelAlcanzado, String Premio,                // CONSTRUCTOR
            Estudiante participante, Estudiante compañero, ArrayList<ComodinEnJuego> comodines, 
            ArrayList<PreguntaEnJuego> preguntas) {
        this.paralelo = paralelo;
        this.fecha = fecha;
        this.preguntasPorNivel = preguntasPorNivel;
        this.nivelAlcanzado = nivelAlcanzado;
        this.Premio = Premio;
        this.participante = participante;
        this.compañero = compañero;
        this.comodines = comodines;
        this.preguntas = preguntas;
    }
               // SETTERS GETTERS
    public Paralelo getParalelo() {
        return paralelo;
    }

    public Date getFecha() {
        return fecha;
    }

    public int getPreguntasPorNivel() {
        return preguntasPorNivel;
    }

    public int getNivelAlcanzado() {
        return nivelAlcanzado;
    }

    public String getPremio() {
        return Premio;
    }

    public Estudiante getParticipante() {
        return participante;
    }

    public Estudiante getCompañero() {
        return compañero;
    }

    public ArrayList<ComodinEnJuego> getComodines() {
        return comodines;
    }

    public ArrayList<PreguntaEnJuego> getPreguntas() {
        return preguntas;
    }
    
    
}
