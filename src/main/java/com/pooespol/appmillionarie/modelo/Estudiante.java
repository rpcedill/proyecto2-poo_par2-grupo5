/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie.modelo;

import java.io.Serializable;
import java.util.Objects;

/**
 * 
 * @author raini
 */
public class Estudiante implements Serializable {               // ATRIBUTOS DE LA CLASE
    private static final long serialVersionUID = 2778396445665453943L;
    private String matricula;
    private String nombre;
    private String email;

    public Estudiante(String matricula) {               // CONSTRUCTOR
        this.matricula = matricula;
    }

    public Estudiante(String matricula, String nombre, String email) {               // CONSTRUCTOR
        this.matricula = matricula;
        this.nombre = nombre;
        this.email = email;
    }
               // SETTERS GETTERS TOSTRING EQUALS
    public String getMatricula() {
        return matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        if (!Objects.equals(this.matricula, other.matricula)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return matricula + ", "+ nombre + ", " + email;
    }

    
    
}
