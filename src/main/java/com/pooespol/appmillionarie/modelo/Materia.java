/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie.modelo;
import com.pooespol.appmillionarie.modelo.Pregunta;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author raini
 */
public class Materia implements Serializable {               // ATRIBUTOS DE LA CLASE
    private static final long serialVersionUID = 2778396445679753943L;
    private String nombre;
    private String codigo;
    private boolean activa;
    private int cantNiveles;
    private ArrayList<Pregunta> preguntas;

    public Materia(String codigo) {               // CONSTRUCTOR
        this.codigo = codigo;
    }
    public Materia(String nombre, String codigo) {               // CONSTRUCTOR
        this.nombre = nombre;
        this.codigo = codigo;
    }
    public Materia(String nombre, String codigo, boolean activa) {               // CONSTRUCTOR
        this.nombre = nombre;
        this.codigo = codigo;
        this.activa = activa;
    }

    public Materia(String nombre, String codigo, boolean activa, int cantNiveles) {               // CONSTRUCTOR
        this.nombre = nombre;
        this.codigo = codigo;
        this.activa = activa;
        this.cantNiveles = cantNiveles;
        this.preguntas = new ArrayList<>();
    }
               // SETTERS GETTERS TOSTRING EQUALS
    public String getNombre() {
        return nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public boolean getActiva() {
        return activa;
    }

    public int getCantNiveles() {
        return cantNiveles;
    }

    public void setPreguntas(ArrayList<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    public ArrayList<Pregunta> getPreguntas() {
        return preguntas;
    }

    @Override
    public String toString() {
        return nombre + "-" + codigo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Materia other = (Materia) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }
    
    
}
