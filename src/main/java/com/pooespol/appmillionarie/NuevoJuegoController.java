/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillionarie;

import com.pooespol.appmillionarie.modelo.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author raini
 */
public class NuevoJuegoController {             // ATRIBUTOS DE LA CLASE Y VENTANA
    private ArrayList<Paralelo> paralelos;
    private ArrayList<TerminoAcademico> terminosAc;
    private ArrayList<Materia> materias;
    private TerminoAcademico termino ;
    private ArrayList<Pregunta> preguntas;
    private ArrayList<Estudiante> estudiantes;
    private Paralelo par;
    private ArrayList<Pregunta> preguntasElegidas;
    @FXML
    private ComboBox cmbParalelos;
    @FXML
    private ComboBox posibleParticipante;
    @FXML
    private ComboBox posibleCompañero;
    @FXML
    private ComboBox preguntasPorNivel;
    @FXML
    private Button botonAceptar;
    @FXML
    private Button botonCancelar;

    @FXML
    private void switchToMenu() throws IOException {             // CAMBIA A MENU
        App.setRoot("primary");
    }
    
    @FXML
    private void mostrarjuego() throws IOException{             // OBTIENE LOS DATOS QUE SE ELIGIERON EN LA VENTANA
        if(preguntasPorNivel.getValue()==null|| cmbParalelos.getValue()==null             // COMPRUEBA Y VALIDA LOS DATOS
                || posibleParticipante.getValue()==null|| posibleCompañero.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setContentText("algun campo esta vacio");
                            alert.show();
        }else{
            FXMLLoader ventanajuego= new FXMLLoader(App.class.getResource("juego.fxml"));             // ASIGNA DATOS AL ROOT
            Parent root =(Parent) ventanajuego.load();
            ControlladorTiempo c2 = ventanajuego.getController();
            c2.setPreguntasJuego(preguntasElegidas);
            int i=(Integer)preguntasPorNivel.getValue();
            c2.setNumeroPreguntas(i*3);
            c2.setParticipante((Estudiante) posibleParticipante.getValue());
            c2.setCompañeroParti((Estudiante) posibleCompañero.getValue());
            App.cambiaRoot(root);             // MANDA A LA VENTANA JUEGO CON LOS DATOS OBTENIDOS
            
        }
        
        
        //App.setRoot("juego");
    }
    
    public NuevoJuegoController() {             // CONSTRUCTOR Y CARGA DE TERMINOS PREGUNTAS MATERIAS PARALELOS ESUTDIANTES TERMINO CONFIGURADO
        Datos d1 = new Datos();
        terminosAc = d1.cargarTerminos();
        preguntas = d1.cargarPreguntas();
        materias = d1.cargarMaterias();
        paralelos = d1.cargarParalelos();
        estudiantes = d1.cargarEstudiantes();
        termino = d1.cargarTerminoJuego();
        System.out.print(d1.cargarTerminoJuego());
        //System.err.print(materias.get(0).getPreguntas());
    }
    
    @FXML
    private void mostrarCompañeros(){             // MUESTRA A LOS COMPAÑEROS DEPENDIENDO DEL PARTICIPANTE QUE SE SELECCIONO
        ArrayList<Estudiante> compas= new ArrayList<>();
        Estudiante es = (Estudiante) posibleParticipante.getValue();
        par = (Paralelo)cmbParalelos.getValue();
        for (Estudiante e : par.getEstudiantes()){
            if (es.equals(e)){
            }else{
                compas.add(e);
            }
        }
        posibleCompañero.getItems().setAll(compas);             // LLENA EL COMBOBOX
    }
    
    @FXML
    private void mostrarEstudiantes() {             // MUESTRA LOS ESTUDIANTES DEPENDIENDO DEL PARALELO SELECCIONADO
        ArrayList<Estudiante> est= new ArrayList<>();
         par = (Paralelo)cmbParalelos.getValue();
         Materia mat = par.getMateria();
        ArrayList<Integer> ints = new ArrayList<>();
        if (par.getEstudiantes()!=null){
            posibleParticipante.getItems().setAll(par.getEstudiantes());
            ArrayList<Pregunta> pre = new ArrayList<>();
            int z = materias.indexOf(mat);
            pre= materias.get(z).getPreguntas();
            int n1 = 0;            int n2 = 0;            int n3 = 0;
            for (Pregunta p: pre){             // CARGA EL MAXIMO Y VALIDA LA CANTIDAD DE PREGUNTAS POR NIVEL
                if(p.getNivel()==1){
                    n1++;
                }
                else if(p.getNivel()==2){
                    n2++;
                }
                else if(p.getNivel()==3){
                    n3++;
                }
            }
            int n=0;    
            if (n1<=n2 &&n1<=n3){
                n=n1;
            }
            if (n2<=n3 &&n2<=n1){
                n=n2;
            }
            if (n3<=n2 &&n3<=n1){
                n=n3;
            }
            for (int i = 1; i <= n;){                
                ints.add(i);
                i++;
            }
            if (n!=0){
            preguntasPorNivel.getItems().setAll(ints);             // LLENA CON LAS POSIVBLES OPCIONES EL COMBOBOX DE PREGUNTAS POR NIVEL
            preguntasElegidas= pre;             // RECIBE LAS PREGUNTAS QUE SE MANDARAN AL JUEGO
            
            }
        }else{
        posibleParticipante.getItems().setAll(est);             // LLENA LOS COMBOBOX
        preguntasPorNivel.getItems().setAll(ints);
        }
    }
    @FXML
    private void initialize() {             // LLENA EL COMBOBOX DE PARALELOS DEPENDIENDO DEL TERMINO CONFIGURADO
        cmbParalelos.getItems().setAll(termino.getParalelos());
    }
            
    
    
     
}
