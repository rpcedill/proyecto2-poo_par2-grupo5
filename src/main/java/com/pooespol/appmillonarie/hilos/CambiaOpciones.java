/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillonarie.hilos;

import com.pooespol.appmillionarie.ControlladorTiempo;
import com.pooespol.appmillionarie.NuevoJuegoController;
import com.pooespol.appmillionarie.modelo.Pregunta;
import com.pooespol.appmillionarie.modelo.PreguntaEnJuego;
import static java.lang.Math.random;
import static java.lang.StrictMath.random;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

/**
 *
 * @author Gustavo
 */
public class CambiaOpciones implements Runnable {             // ATRIBUTOS DEL HILO
    private Pregunta pregunta;
    private Label lblpregunta;
    private GridPane grdrespuestas;
    private int contador =0;
    private int totalpreguntas;
    private int nextquestion;
    private int ent=0;
    private int puntos = 0;
    private Button starton;
    private boolean progresion = true;
    TextFlow txtflow;
    Temporizador t;
    private Button a;
    private Button b;
    private Button c;
    private Button d;
    
    public CambiaOpciones(Pregunta pregunt,Label lblpregunta,GridPane grdrespuestas,int nextquestion,             // CONSTRUCTOR
            Button starton, TextFlow txtflow, int totalpreguntas, Button a,Button b,Button c,Button d,int punt){
        this.pregunta=pregunt;
        this.lblpregunta=lblpregunta;
        this.grdrespuestas=grdrespuestas;
        this.nextquestion=nextquestion;
        this.starton=starton;
        this.txtflow=txtflow;
        this.totalpreguntas=totalpreguntas;
        //this.t=t;
        this.a=a;
        this.b=b;
        this.c=c;
        this.d=d;
        this.puntos =punt;
        cargarPreguntas();
       
        
    }
    
    @Override
    public void run(){             // METODO RUN
        while (progresion) {
            try {
                Thread.sleep(1000);//duerme 1 segundo
            } catch (InterruptedException ex) {
            }
            Platform.runLater(() -> {
                incrementador();             // LLAMA A INCREMENTAR
                

            });

        }
            
    }
    public void cargarPreguntas(){             // CARGA LAS PREGUNTAS UNA Y ORTA VEZ EN EL AVANCE DE PREGUNTAS
        txtflow.getChildren().clear();             // LIMIPIA EL TEXT FLOW
                    VBox orden2 = new VBox();
                    Label lblpregunta2 = new Label("AVANCE PREGUNTAS");
                    orden2.getChildren().add(lblpregunta2);
                    lblpregunta2.setPrefSize(300,50);
                    lblpregunta2.setWrapText(true);
                    lblpregunta2.setTextFill(Color.BLACK);
                    lblpregunta2.setAlignment(Pos.CENTER);
                    lblpregunta2.setTextAlignment(TextAlignment.CENTER);
                    lblpregunta2.setStyle("-fx-border-color:cyan;-fx-background-color:#f0ff71;");
                    txtflow.getChildren().add(orden2);
        
        for (int i =totalpreguntas+1; i>1 ;i--){             // ASIGNA UN LABEL POR EL NUMERO DE PREGUNTAS TOTALES DEL JUEGO
                    VBox orden = new VBox();
                    //orden.setPrefSize(360,60);
                    Label lblpregunta = new Label("Pregunta #"+String.valueOf(i-1));
                    orden.getChildren().add(lblpregunta);
                    lblpregunta.setPrefSize(300,20);
                    lblpregunta.setWrapText(true);
                    if(i-1<=puntos){
                        lblpregunta.setStyle("-fx-border-color:cyan;-fx-background-color:GREEN;");             // CAMBIA DE COLOR SI YA SE CONTESTO LA PREGUNTA
                    }else{
                        lblpregunta.setStyle("-fx-border-color:cyan;-fx-background-color:WHITE;");
                    }
                    lblpregunta.setTextFill(Color.BLACK);
                    lblpregunta.setAlignment(Pos.CENTER);
                    lblpregunta.setTextAlignment(TextAlignment.CENTER);
                    txtflow.getChildren().add(orden);

        }
    }
    public void incrementador(){             // ASGINA LOS BOTONES RESPUESTAS Y PREGUNTA
        
        contador++;
            
            if(contador==1 &&ent==0){
                Platform.runLater(() -> {
                    grdrespuestas.getChildren().clear();             // LIMPIA EL GRID
                    lblpregunta.setText(pregunta.getEnunciado());
                    ArrayList<String> posiblesresp = pregunta.getPosiblesRespuestas();
                    a.setDisable(false);
                    b.setDisable(false);             // HABILITA LOS BOTONES
                    c.setDisable(false);
                    d.setDisable(false);
                    Random rt = new Random();
                    int indice = rt.nextInt(4);             // ESCOGE UN NUMERO RANDOM DE 0 A 3
                    if(indice==0){             // SE POSICIONA LA RESPUESTAS CORRECTAS E INCORRECTAS DE MANERA ALEATORIA EN LOS BOTONES
                    a.setText("A "+posiblesresp.get(0));
                    b.setText("B "+posiblesresp.get(1));
                    c.setText("C "+posiblesresp.get(2));
                    d.setText("D "+pregunta.getRespuestaCorrecta());
                    }else if(indice==1){
                    a.setText("A "+posiblesresp.get(0));
                    b.setText("B "+posiblesresp.get(2));
                    c.setText("C "+pregunta.getRespuestaCorrecta());
                    d.setText("D "+posiblesresp.get(1));
                    }else if(indice==2){
                    a.setText("A "+posiblesresp.get(2));
                    b.setText("B "+pregunta.getRespuestaCorrecta());
                    c.setText("C "+posiblesresp.get(1));
                    d.setText("D "+posiblesresp.get(0));
                    }else{
                    a.setText("A "+pregunta.getRespuestaCorrecta());
                    b.setText("B "+posiblesresp.get(1));
                    c.setText("C "+posiblesresp.get(2));
                    d.setText("D "+posiblesresp.get(0));
                    }   
                    grdrespuestas.add(a, 0, 0);             // SE COLOCAN EN EL GRID
                    grdrespuestas.add(b, 1, 0);
                    grdrespuestas.add(c, 0, 1);
                    grdrespuestas.add(d,1, 1);
                    
                    
                    
                    ent++;
                    
                    });
                
            }
            /*if(contador==30){
                progresion=false;
                contador=0;
                starton.setDisable(false);
                a.setDisable(true);
                b.setDisable(true);
                c.setDisable(true);
                d.setDisable(true);
                //System.out.println(nextquestion+"-->next");
                //System.out.println(totalpreguntas+"-->total");
                if(nextquestion+1==totalpreguntas){
                    starton.setDisable(true);
                }


            }*/
            
            
        
        
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CambiaOpciones other = (CambiaOpciones) obj;
        if (!Objects.equals(this.a, other.a)) {
            return false;
        }
        if (!Objects.equals(this.b, other.b)) {
            return false;
        }
        if (!Objects.equals(this.c, other.c)) {
            return false;
        }
        if (!Objects.equals(this.d, other.d)) {
            return false;
        }
        return true;
    }

    public void setProgresion(boolean progresion) {             // CONFIGURA LA PROGRESION
        this.progresion = progresion;
    }
    
    
    
    
}
