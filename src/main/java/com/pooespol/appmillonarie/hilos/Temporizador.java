/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pooespol.appmillonarie.hilos;

import com.pooespol.appmillionarie.App;
import com.pooespol.appmillionarie.modelo.Pregunta;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 *
 * @author Gustavo
 */
public class Temporizador implements Runnable{             // ATRIBUTOS DEL HILO
    private int contador=0;
    private Label texto;
    private int ent=0;
    private int i=0;
    private Button btnsalirjuego;
    private Button btncomenzar;
    boolean progresion = true;
    private Button a;
    private Button b;
    private Button c;
    private Button d;
    ImageView imagenV;
    Image imagen;
    public Temporizador(){}             // CONSTRUCTOR 
    public Temporizador(Label texto,ImageView imagenV, Button boton,Button boton2,Button a,Button b,Button c,Button d){
        this.texto=texto;
        this.imagenV=imagenV;
        this.btnsalirjuego=boton;
        this.btncomenzar=boton2;
        this.a=a;
        this.b=b;
        this.c=c;
        this.d=d;
        
    }
    
    public void incrementador(){                     // METODO PARA INCREMENTAR EL TIEMPO
        contador++;
        texto.setText(String.valueOf(contador));             // ACTUALIZA EL TIEMPO
        if(contador==60&& ent==0){
            progresion=false;
            btnsalirjuego.setDisable(false);
            contador--;           
            System.out.println("se termino el tiempo");
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);             // ALERTA SIN TIEMPO
                            alert.setContentText("tiempo agotado");
                            alert.show();
                            btncomenzar.setDisable(true);
                            a.setDisable(true);             // DESABILITA LSO BOTONES
                            d.setDisable(true);
                            b.setDisable(true);
                            c.setDisable(true);
                            ent++;                         
            });
            imagenV.setVisible(false);               // SE QUITA LA IMAGEN DE TIEMPO           
                        
        }
        if(contador==50){
            FileInputStream inputImagen=null;
        
            try{
                inputImagen = new FileInputStream(App.pathimagen+"alarma2.png");              // SE AÑADE NUEVA IMAGEN           
                 imagen= new Image(inputImagen, 150,150,false,false);
                imagenV.setImage(imagen);

            }catch(FileNotFoundException exc){
                exc.printStackTrace();

            }finally{
                try{
                    inputImagen.close();
                }catch(IOException ex){
                    System.out.print("No se ha cerrado el archivo ");
                }

            }
            imagenV.setVisible(true);
        }
        
        
    }
    @Override
    public void run() {             // METODO RUN
        while (progresion) {
            try {
                Thread.sleep(1000);//duerme 1 segundo
            } catch (InterruptedException ex) {
            }
            Platform.runLater(() -> {              
                incrementador();             // LLAMA A INCREMENTADOR
                
            });

        }
    }  

    public void setProgresion(boolean progresion) {             // CONFIGURA LA PROGRESION
        this.progresion = progresion;
    }

    public void setContador(int contador) {             // CONFIGURA EL CONTADOR
        this.contador = contador;
    }
    
    
}
